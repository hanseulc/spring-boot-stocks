package com.citi.training.stocks.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Stock {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String ticker;
	private String companyName;
	
	//default no-args constructor
	public Stock() {}
	
	//constructor taking id,ticker,companyName
	public Stock(long id, String ticker, String companyName) {
		this.id = id;
		this.ticker = ticker;
		this.companyName = ticker;
	}
	
	//getters and setters
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
